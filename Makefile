SPECFILE             = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
DIST                ?= $(shell rpm --eval %{dist})

clean:
	rm -rf build/ *.tgz

sources:
	mkdir -p source/$(SPECFILE_NAME)-$(SPECFILE_VERSION)
	rsync -r --exclude 'source' . source/$(SPECFILE_NAME)-$(SPECFILE_VERSION)/
	tar  -C source -cvzf source/$(SPECFILE_NAME)-$(SPECFILE_VERSION).tar.gz $(SPECFILE_NAME)-$(SPECFILE_VERSION)

rpm: sources
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/source' $(SPECFILE)

srpm: sources
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/source' $(SPECFILE)
